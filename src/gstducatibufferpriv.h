/*
 * GStreamer
 *
 * Copyright (C) 2012 Texas Instruments
 * Copyright (C) 2012 Collabora Ltd
 *
 * Authors:
 *  Alessandro Decina <alessandro.decina@collabora.co.uk>
 *  Rob Clark <rob.clark@linaro.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __GSTDUCATIBUFFERPRIV_H__
#define __GSTDUCATIBUFFERPRIV_H__

#include <stdint.h>
#include <gst/gst.h>

G_BEGIN_DECLS

#include <gst/dmabuf/dmabuf.h>

/*
 * per-buffer private data for gst-ducati
 */

#define GST_TYPE_DUCATI_BUFFER_PRIV      \
  (gst_ducati_buffer_priv_get_type ())
#define GST_DUCATI_BUFFER_PRIV(obj)      \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_DUCATI_BUFFER_PRIV, GstDucatiBufferPriv))
#define GST_IS_DUCATI_BUFFER_PRIV(obj)     \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_DUCATI_BUFFER_PRIV))

typedef struct _GstDucatiBufferPriv      GstDucatiBufferPriv;
typedef struct _GstDucatiBufferPrivClass GstDucatiBufferPrivClass;


struct _GstDucatiBufferPriv
{
  GstMiniObject parent;

  struct omap_bo *bo;
  gint uv_offset, size;
};

struct _GstDucatiBufferPrivClass
{
  GstMiniObjectClass parent_class;
};


GType gst_ducati_buffer_priv_get_type (void);

void gst_ducati_buffer_priv_set (GstBuffer * buf, GstDucatiBufferPriv * priv);
GstDucatiBufferPriv * gst_ducati_buffer_priv_get (GstBuffer * buf);
GstDucatiBufferPriv * gst_ducati_buffer_priv_new (void);


G_END_DECLS


#endif /* __GSTDUCATIBUFFERPRIV_H__ */
