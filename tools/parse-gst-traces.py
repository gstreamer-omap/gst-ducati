#! /usr/bin/python

# GStreamer scheduling parser
# Analyze GST_SCHEDULING traces and:
# - Display a summary (x frames during y seconds, that is z fps)
# - Display details (the time spent by each buffer at each GST element level)
# - Draw a graph (using matplotlib)
#
# Usage:
# $ GST_DEBUG=*:2,GST_SCHEDULING:5,GST_PERFORMANCE:5 GST_DEBUG_NO_COLOR=1 DISPLAY=:0 gst-launch-0.10 -e [PIPELINE] >foo 2>&1
# $ ./tools/parse-gst-traces.py [ARGS] foo
#
# Example of PIPELINE: videotestsrc num-buffers=200 horizontal-speed=1 ! "video/x-raw-yuv, format=(fourcc)NV12, width=1280, height=720, framerate=30/1" ! ducatih264enc profile=66 ! ducatih264dec! dri2videosink sync=false
#
# Example of ARGS: --from 100 --to 148 --no-plot --no-details --ignore rtpbin0

import sys
import time
import fileinput
import re
import operator
import datetime
import argparse

# Try to import matplotlib, and enable graph on success
plotlib = True
try:
  import numpy as np
  import matplotlib.pyplot as plt
except:
  plotlib = False

def string_to_time (d):
  return datetime.datetime.strptime(d, "%H:%M:%S.%f")

def time_to_seconds (d):
  return datetime.timedelta( hours=d.hour, minutes=d.minute, seconds=d.second, microseconds=d.microsecond ).total_seconds()

def main():
  global plotlib
  tr = dict()
  cam = dict()
  buf = dict()
  ecs = 0

  ### Parse command line
  parser = argparse.ArgumentParser()
  parser.add_argument('--start', type=int, default=1)
  parser.add_argument('--end', type=int, default=sys.maxint)
  parser.add_argument('--no-plot', action='store_false', dest='plot')
  parser.add_argument('--no-details', action='store_false', dest='details')
  parser.add_argument('--ignore', action='append', default=[])
  parser.add_argument('files', nargs='*', default='-')
  args = parser.parse_args()
  args.plot &= plotlib

  ### Regular expressions for traces
  gst_header_traces = re.compile (r"([0-9:.]{14})[0-9]{3}[\s]+([0-9]+)[\s]+([x0-9A-Fa-f]+)[\s]+([A-Z]+)[\s]+(.*)")
  gst_scheduling_traces = re.compile (r"GST_SCHEDULING gstpad.c:[\w]+:([\w_]+):<([\w_]+):([\w_]+)> calling chainfunction &[\w_]+ with buffer ([\w]+), data ([\w]+), malloc ([\w()]+), ts ([0-9:.]{14})[0-9]{3}, dur ([\w:.]+)")
  omx_camera_traces = re.compile (r"GST_PERFORMANCE gstomx_core.c:[\w]+:FillBufferDone:<cam> FillBufferDone: GstBuffer=([\w]+)")
  ducati_encoder_traces = re.compile (r"GST_PERFORMANCE gstducatividenc.c:[\w]+:gst_ducati_videnc_handle_frame:<ducati[a-z0-9]*enc[\w]+> Encoded frame in ([\w]+) bytes")
  dri2sink_traces = re.compile (r"GST_PERFORMANCE gstdri2util.c:[\w]+:gst_dri2window_buffer_show:<([\w_]+)> (Before DRI2SwapBuffersVid|After DRI2WaitSBC), buf=([\w]+)")

  ### Parse stdin or any file on the command line
  for line in fileinput.input(args.files):

    # Filter out GST traces from other output
    m = re.match(gst_header_traces,line)
    if m == None:
      continue
    (ts_ev,pid,adr,sev,msg) = m.groups()

    # Parse GST_SCHEDULING traces
    m = re.match(gst_scheduling_traces, msg)
    if m != None:
      (func, elemNum, pad, gstBuffer, data, malloc, ts_fr, dur) = m.groups()
      if func !=  "gst_pad_push" or elemNum in args.ignore:
        continue
      if elemNum == "cam" and pad == "src" and gstBuffer in cam:
        tr[ts_fr] = [(cam[gstBuffer], "omx_cam", "FillBufferDone")]
      buf[gstBuffer] = ts_fr
      if ts_fr not in tr:
        tr[ts_fr] = []
      tr[ts_fr].append((ts_ev, elemNum, "%s:<%s:%s>" % (func, elemNum, pad) ))
      continue

    # Parse omx_camera traces
    m = re.match(omx_camera_traces, msg)
    if m != None:
      gstBuffer = m.group(1)
      cam[gstBuffer] = ts_ev
      continue

    # Parse ducati encoder traces
    m = re.match(ducati_encoder_traces, msg)
    if m != None:
       ecs += int(m.group(1))
       continue

    # Parse dri2videosink traces
    m = re.match(dri2sink_traces, msg)
    if m != None:
      (elemNum, mess, dri2_buf) = m.groups()
      if dri2_buf in buf and buf[dri2_buf] in tr:
        tr[buf[dri2_buf]].append((ts_ev, elemNum, mess))
      continue

  ### End of stdin parsing

  if args.plot:
    # Prepare for plotting
    fig = plt.figure()
    ax = fig.add_subplot(111)
    legends = list()
    colors = "bgrcmykw"

  # Display the results, frame per frame
  grandtotal = 0.0
  cnt = 0
  for ts_fr in sorted(tr):
    cnt +=1
    if cnt < args.start or cnt > args.end:
      continue
    events=tr[ts_fr]
    first = prev = string_to_time(events[0][0])
    if cnt == args.start:
      very_first = first
    if args.details:
      print "\n*** Frame no: %d, timestamp: %s" % (cnt, ts_fr)

    for event in events:
      cur = string_to_time(event[0])
      if args.details:
        if cur != prev:
          later = "(%6d us later)" % ((cur - prev).total_seconds() * 1000000)
        else:
          later = "(    first event)"
        print "At %s %s Func: %s" % (event[0], later, event[2])
      if args.plot:
        if event[1] not in legends:
          legends.append(event[1])
        color = colors[legends.index(event[1]) % len(colors)]
        ax.plot(time_to_seconds(cur), cnt, color + 'o')
      prev = cur

    total = (cur - first).total_seconds()
    grandtotal += total
    if args.details:
      print "*** Total: %6d us" % (total * 1000000)

  # Display the totals
  if (cnt != 0):
    args.end = min(args.end, cnt)
    cnt = args.end - args.start + 1
    duration = (cur - very_first).total_seconds()
    print "\n=-= %d frames during %s seconds, that is %2.2f fps" \
          % (cnt, duration, cnt / duration)
    print "\n=-= Average latency: %6d us" % (grandtotal * 1000000 / cnt)
    if (ecs != 0):
      ecs = "%s bytes" % (ecs / cnt)
    else:
      ecs = "N/A (lack of encoder traces)"
    print "\n=-= Average size of encoder buffer: %s" % (ecs)
  else:
    print "\n=-= No frame, the pipeline have failed"

  if args.plot:
    # And plot it
    plt.legend(legends, loc=0, title='GST elements')
    plt.xlabel('Time in seconds')
    plt.ylabel('Buffer id')
    ax.set_title('GStreamer pipeline')
    plt.show()

  return 0
if __name__ == '__main__':
  main()



